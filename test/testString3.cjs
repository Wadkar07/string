const string3= require('../string3.cjs')
var expect = require('chai').expect;

describe('Problem 3',()=>{
    it('Returns Month ',()=>{
        expect(string3('10/1/2021')).to.equal('January');
    })
    it('Returns empty string',()=>{
        expect(string3()).to.equal(' ');
    })
    it('Returns empty string',()=>{
        expect(string3('')).to.equal(' ');
    })
    it('Returns empty string',()=>{
        expect(string3('asdxcsc')).to.equal(' ');
    })
})