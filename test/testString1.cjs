const string1 = require('../string1.cjs')
var expect = require('chai').expect;

describe('Problem 1',()=>{
    it('Returns number with removed \'$\' and \',\'',()=>{
        expect(string1('$123')).to.equal(123);
    })
    it('Returns empty string ',()=>{
        expect(string1('123')).to.equal(0);
    })
})