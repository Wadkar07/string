var expect = require('chai').expect;

const string4 = require('../string4.cjs')

describe('Problem 4 ',()=>{
    it('Return full name',()=>{
        expect(string4({"first_name": "JoHN", "middle_name": "doe", "last_name": "SMith"})).to.equal('John Doe Smith ');
    })
    it('Return empty string',()=>{
        expect(string4("first_name")).to.equal('');
    })

    it('Return empty string',()=>{
        expect(string4()).to.equal('');
    })
})