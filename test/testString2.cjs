const string2 = require('../string2.cjs')
var expect = require('chai').expect;

describe('Problem 2',()=>{
    it('Returns component of IPV4 ip address',()=>{
        expect(string2('111.139.61.143')).to.eql([ 111, 139, 61, 143 ]);
    })
    it('Returns empty array',()=>{
        expect(string2('123')).to.eql([]);
    })
    it('Returns empty array',()=>{
        expect(string2('')).to.eql([]);
    })
    it('Returns empty array',()=>{
        expect(string2('123.123s.123.123')).to.eql([]);
    })
})