const string5 = require('../string5.cjs')
var expect = require('chai').expect;

describe('Problem 5',()=>{
    it('Returns combined string',()=>{
        expect(string5(["this", "quick", "brown", "fox"])).to.equal('this quick brown fox ');
    })
    it('Returns empty string',()=>{
        expect(string5('123')).to.equal('');
    })
    it('Returns empty string',()=>{
        expect(string5()).to.equal('');
    })
})