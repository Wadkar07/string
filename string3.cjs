// ==== String Problem #3 ====
// Given a string in the format of "10/1/2021", print the month in which the date is present in.

let month = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
function string3(date){
    if(date === undefined){
        return ' ';
    }
    let validRegex = /[0-9]\/[0-9]+\/[0-9]/;
    let dateArray=date.split('/');
    if(dateArray.length>3||dateArray[1]>12 || dateArray[1]<1||!date.match(validRegex)){
        return ' ';
    }
    return month[dateArray[1]-1];
 
}
console.log(string3("10/1/2021"))
module.exports = string3;