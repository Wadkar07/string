// ==== String Problem #4 ====
// Given an object in the following format, return the full name in title case.
// {"first_name": "JoHN", "last_name": "SMith"}
// {"first_name": "JoHN", "middle_name": "doe", "last_name": "SMith"}

function string4(nameObj){
    if(nameObj=== undefined || typeof nameObj !== 'object'){
        return ""
    }
    let name = Object.values(nameObj);
    let fullName= '';
    for(let i=0;i<name.length;i++){
        let partialName = name[i];
        name[i] = partialName.charAt(0).toUpperCase()+partialName.slice(1).toLowerCase();
        fullName += name[i]+" "
    }
    return fullName;
}
let fullName = string4({"first_name": "JoHN", "middle_name": "doe", "last_name": "SMith"});
console.log(fullName);
module.exports = string4;