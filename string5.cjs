// ==== String Problem #5 ====
// Given an array of strings ["the", "quick", "brown", "fox"], convert it into a string "the quick brown fox."
// If the array is empty, return an empty string.

function string5(stringArray){
    if(!Array.isArray(stringArray)){
        return '';
    }
    let str = '';
    for(let index=0;index < stringArray.length;index++){
        if(typeof stringArray[index] !== 'string'){
            return '';
        }
        str += stringArray[index]+' ';
    }
    return str;
}
console.log(string5(["this", "quick", "brown", "fox"]))
module.exports = string5;