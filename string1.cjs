// // ==== String Problem #1 ====
// // There are numbers that are stored in the format "$100.45", "$1,002.22", "-$123", and so on. Write a function to convert the given strings into their equivalent numeric format without any precision loss - 100.45, 1002.22, -123 and so on. There could be typing mistakes in the string so if the number is invalid, return 0. 

function string1(string){
    let validRegex = /^[+-]+\$[0-9.,]/;
    let validRegex1 = /[$]+[0-9.,]/;
    let num = string.split('$');
    let amount = 0;
    if (string.match(validRegex)||string.match(validRegex1)) {
        if(string.includes('.')){
            let decimalArray = num[1].split('.');
            if(decimalArray.length > 2 || decimalArray[1].includes(',')) {
                return 0;
            }
        }
        string= string.replace(',','').replace('$','');
        amount = Number(string);
        if(isNaN(amount)){
            return 0;
        }
        // console.log(typeof amount+" "+amount);
    }
    return amount;
}
console.log(string1("$123,45.21"));

module.exports = string1;