// ==== String Problem #2 ====
// Given an IP address - "111.139.161.143". Split it into its component parts 111, 139, 161, 143 and return it in an array in numeric values. [111, 139, 161, 143].

// Support IPV4 addresses only. If there are other characters detected, return an empty array.
function string2(string){
    let componentArray=[];
    if(string === undefined){
        return componentArray;
    }
    components=string.split('.');
    if(components.length===4){
        for(let i=0;i<components.length;i++){
            if(isNaN(components[i])){
                return [];
            }
            componentArray.push(Number(components[i]))
        }
    }
    return componentArray;
}

let ip = "111.139.61.143"
console.log(string2(ip));
module.exports = string2;
